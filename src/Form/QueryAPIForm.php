<?php

/**
 * @file
 * @file_
 * Contains \Drupal\ld_tool\Form\QueryAPIForm.
 */

namespace Drupal\ld_tool\Form;

use Drupal\Component\Utility\String;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\ld_tool\FreebaseQueryHandler;
use Drupal\ld_tool\WikidataQueryHandler;
/**
 *
 */
class QueryAPIForm extends FormBase {

  protected $dataSources;
  /**
   * Constructs a new ContentBuilder.
   */
  public function __construct() {
    $this->dataSources = array(
      'freebase' => new FreebaseQueryHandler(),
      'wikidata' => new WikidataQueryHandler(),
    );
  }


  /**
   * @inheritdoc
   */
  public function getFormId() {
    return "ld_tool_query_api_form";
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->t('Search API');
    $form['description'] = array(
      '#type' => 'item',
      '#title' => $this->t('Query an API'),
    );

    $form['API'] = array(
      '#title' => $this->t('Data Source'),
      '#id' => 'data-source',
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $this->getListTypes(),
      '#empty_option' => '',
      '#description' => $this->t('Select Knowledge Base'),
      '#attributes' => array(
        'class' => array('ld-selector'),
      ),
      '#attached' => array(
        'library' => array(
          'ld_tool/drupal.ld_tool.dropdown',
        ),
      ),
    );

    $form['entity'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Entity'),
      '#size' => 60,
      '#description' => $this->t('Entity to search for'),
      '#required' => TRUE,
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['next'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    );

    $form['result'] = array(
      '#title' => t('Result'),
      '#type' => 'textarea',
      '#description' => t('Append results.'),
    );

    return $form;
  }

  /**
   * @inheritdoc
   *
   * Final submit handler- gather all data together and create new content type.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->disableRedirect(TRUE);
    $source = $this->dataSources[$form_state->getValue('API')];
    $output = $source->queryForEntity($form_state->getValue('entity'));

    var_dump($output);
    drupal_set_message(t('Content Searched. %response', array('%response' => "hello")));
  }

  /**
   *
   */
  private function getListTypes() {
    $values = array(
      "freebase" => "Freebase",
      "wikidata" => "WikiData",
    );

    return $values;
  }

}
