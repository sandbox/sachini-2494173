<?php

/**
 * @file
 * Contains \Drupal\ld_tool\Form\DataSourceConfigForm.
 */

namespace Drupal\ld_tool\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Linked Data Source settings for this site.
 */
class DataSourceConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ld_tool_datasource_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ld_tool.api_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $ld_config = $this->config('ld_tool.api_key');
    $form['freebase'] = array(
      '#type' => 'textfield',
      '#title' => t('Freebase API key'),
      '#default_value' => $ld_config->get('freebase'),
      '#description' => t('API key for Freebase.(<a href=https://developers.google.com/freebase/v1/getting-started#api-keys>see</a> for more details)'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ld_tool.api_key')
      ->set('freebase', $form_state->getValue('freebase'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
