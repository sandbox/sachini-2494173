<?php
/**
 * @file
 * Contains \Drupal\ld_tool\Plugin\Field\FieldWidget\LinkedDataWidget.
 */

namespace Drupal\ld_tool\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'ld_tool' widget.
 *
 * @FieldWidget (
 *   id = "ld_tool",
 *   label = @Translation("Linked Data format"),
 *   field_types = {
 *     "ld_tool"
 *   },
 *   settings = {
 *     "placeholder" = "Enter a Linked Data URI"
 *   }
 * )
 */
class LinkedDataWidget extends WidgetBase {


  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'uri' => '',
      'source' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta,
                              array $element, array &$form, FormStateInterface $form_state) {
    $data_source = $this->getFieldSetting('data_source');
    $route = get_knowledge_base_route($data_source);
    $url = Url::fromRoute($route);
    // Echo $url->toUriString();
    $element['uri'] = array(
      '#type' => 'select',
      '#title' => $this->fieldDefinition->getLabel(),
      '#default_value' => $items[$delta]->uri ?: NULL,
      '#validated' => TRUE,
      '#attached' => array(
        'library' => array(
          'ld_tool/drupal.ld_tool.autocomplete',
        ),
        'drupalSettings' => array(
          'ld_tool' => array(
            'url' => substr($url->toString(), 1),
            'placeholder' => $this->getSetting('placeholder'),
            'defaultVal' => $items[$delta]->uri ?: NULL,
          ),
        ),
      ),
      '#attributes' => array(
        'class' => array('ld-autocomplete'),
      ),
    );

    return $element;
  }

}
