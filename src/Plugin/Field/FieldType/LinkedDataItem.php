<?php
/**
 * @file
 * Contains \Drupal\ld)tool\Plugin\Field\FieldType\LinkedDataItem.
 */

namespace Drupal\ld_tool\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemBase;

/**
 * Plugin implementation of the 'ld_tool' field type.
 *
 * @FieldType (
 *   id = "ld_tool",
 *   label = @Translation("LinkedData"),
 *   description = @Translation("Stores a LinkedData URI."),
 *   default_widget = "ld_tool",
 *   default_formatter = "ld_tool"
 * )
 */
class LinkedDataItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = array(
      'uri' => array(
        'type' => 'varchar',
        'length' => 255,
      ),
      'source' => array(
        'type' => 'varchar',
        'length' => 255,
      ),
    );

    return array(
      'columns' => $columns,
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['uri'] = DataDefinition::create('string')
        ->setLabel(t('Linked Data URI'))
        ->setDescription(t('Represents a URI from Knowledge Base'));

    $properties['source'] = DataDefinition::create('string')
        ->setLabel(t('Data Source'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('uri')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'uri';
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = array();
    $source = $this->getFieldDefinition()->getFieldStorageDefinition()->getSetting('data_source');

    $element['data_source'] = array(
      '#type' => 'select',
      '#title' => t('Linked Data Source'),
      '#options' => get_knowledge_bases(),
      '#default_value' => $source,
      '#required' => TRUE,
      '#description' => t('Data source for the field'),
      '#disabled' => $has_data,
      '#attributes' => array(
        'class' => array('ld-selector'),
      ),
      '#attached' => array(
        'library' => array(
          'ld_tool/drupal.ld_tool.dropdown',
        ),
      ),
    );
    $element += parent::storageSettingsForm($form, $form_state, $has_data);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'data_source' => 'freebase',
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $this->source = $this->getFieldDefinition()->getFieldStorageDefinition()->getSetting('data_source');
  }

}
