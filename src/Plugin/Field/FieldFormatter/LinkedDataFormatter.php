<?php
/**
 * @file
 * Contains \Drupal\ld_tool\Plugin\Field\FieldFormatter\LinkedDataFormatter.
 */

namespace Drupal\ld_tool\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'ld_tool' formatter.
 *
 * @FieldFormatter (
 *   id = "ld_tool",
 *   label = @Translation("Linked Data format"),
 *   field_types = {
 *     "ld_tool"
 *   }
 * )
 */
class LinkedDataFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $element = array();

    foreach ($items as $delta => $item) {
      list($postfix, $title) = explode('] ', substr($item->uri, 1));
      $handler = ld_tool_handler_class($items->getSetting('data_source'));
      $instance = new $handler();

      $markup = "<a href=" . $instance->getUrl($postfix) . ">$title</a>";
      $element[$delta] = array(
        '#markup' => $markup,
      );
    }

    return $element;
  }

}
