<?php

/**
 * @file
 * Contains \Drupal\ld_tool\Tests\LinkedDataItemTest.
 */

namespace Drupal\ld_tool\Tests;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Tests\FieldUnitTestBase;

/**
 * Tests the new entity API for the linked data field type.
 *
 * @group Linked_Data
 */
class LinkedDataItemTest extends FieldUnitTestBase {

  public static $modules = array('ld_tool');
  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Create a Linked data field and storage for validation.
    FieldStorageConfig::create(array(
      'field_name' => 'field_linked_data',
      'entity_type' => 'entity_test',
      'type' => 'ld_tool',
    ))->save();
    FieldConfig::create(array(
      'entity_type' => 'entity_test',
      'field_name' => 'field_linked_data',
      'bundle' => 'entity_test',
      'settings' => array(
        'data_source' => 'freebase',
      ),
    ))->save();

    // Create a form display for the default form mode.
    entity_get_form_display('entity_test', 'entity_test', 'default')
      ->setComponent('field_linked_data', array(
        'type' => 'ld_tool',
      ))
      ->save();
  }

  /**
   * Tests using entity fields of the linked data field type.
   */
  public function testLinkedDataItem() {
    // Verify entity creation.
    $entity = entity_create('entity_test');
    $uri = '[/m/123456] Tests';
    $source = 'freebase';
    $entity->field_linked_data = $uri;
    $entity->name->value = $this->randomMachineName();
    $entity->save();

    // Verify entity has been created properly.
    $id = $entity->id();
    $entity = entity_load('entity_test', $id);
    $this->assertTrue($entity->field_linked_data instanceof FieldItemListInterface, 'Field implements interface.');
    $this->assertTrue($entity->field_linked_data[0] instanceof FieldItemInterface, 'Field item implements interface.');
    $this->assertEqual($entity->field_linked_data->uri, $uri);
    $this->assertEqual($entity->field_linked_data[0]->uri, $uri);
    $this->assertEqual($entity->field_linked_data->source, $source);
    $this->assertEqual($entity->field_linked_data[0]->source, $source);

    // Verify changing the Uri value.
    $new_uri = '[Q678] Tests Changed';
    $entity->field_linked_data->uri = $new_uri;
    $this->assertEqual($entity->field_linked_data->uri, $new_uri);

    // Read changed entity and assert changed values.
    $entity->save();
    $entity = entity_load('entity_test', $id);
    $this->assertEqual($entity->field_linked_data->uri, $new_uri);
  }

}
