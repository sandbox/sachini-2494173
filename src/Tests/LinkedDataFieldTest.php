<?php

/**
 * @file
 * Contains \Drupal\ld_tool\Tests\LinkedDataFieldTest.
 */

namespace Drupal\ld_tool\Tests;

use Drupal\Component\Utility\Unicode;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\simpletest\WebTestBase;

/**
 * Tests ld_tool field functionality.
 *
 * @group Linked_Data
 */
class LinkedDataFieldTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array(
    'entity_test',
    'field_ui',
    'options',
    'ld_tool',
  );

  /**
   * A field to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * The field used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {

    parent::setUp();

    $this->drupalLogin($this->drupalCreateUser(array(
        'view test entity',
        'administer entity_test content',
        'administer entity_test form display',
        'administer entity_test fields',
    )));
  }

  /**
   * Tests ld_tool field.
   */
  protected function testLinkedDataField() {

    $label = $this->randomMachineName();

    // Create a field with settings to validate.
    $field_name = Unicode::strtolower($this->randomMachineName());
    $this->fieldStorage = FieldStorageConfig::create(array(
        'field_name' => $field_name,
        'entity_type' => 'entity_test',
        'type' => 'ld_tool',
    ));

    $this->fieldStorage->save();
    $this->field = FieldConfig::create(array(
        'field_name' => $field_name,
        'entity_type' => 'entity_test',
        'bundle' => 'entity_test',
        'label' => $label,
        'required' => TRUE,
        'settings' => array(
          'data_source' => 'freebase',
        ),
    ));
    $this->field->save();

    // Create a form display for the default form mode.
    entity_get_form_display('entity_test', 'entity_test', 'default')
        ->setComponent($field_name, array(
            'type' => 'ld_tool',
        ))
        ->save();

    // Create a display for the full view mode.
    entity_get_display('entity_test', 'entity_test', 'full')
        ->setComponent($field_name, array(
            'type' => 'ld_tool',
        ))
        ->save();

    // Display creation form.
    $this->drupalGet('entity_test/add');
    $this->assertFieldByName("{$field_name}[0][uri]", '', 'Widget found.');

    // Submit and ensure it is accepted.
    $edit = array(
      "{$field_name}[0][uri]" => '[/m/12354] TEST',
    );

    $this->drupalPostForm(NULL, $edit, t('Save'));
    preg_match('|entity_test/manage/(\d+)|', $this->url, $match);
    $id = $match[1];
    $this->assertText(t('entity_test @id has been created.', array('@id' => $id)));

    // Test if we can change the data_source
    $this->drupalGet('entity_test/structure/entity_test/fields/entity_test.entity_test.' . $field_name . '/storage');
    $this->assertText('There is data for this field in the database. The field settings can no longer be changed.', 'Editing Field setting denied since there\'s data in field');
  }

}
