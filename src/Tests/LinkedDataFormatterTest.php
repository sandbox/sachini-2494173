<?php

/**
 * @file
 * Contains \Drupal\ld_tool\Tests\LinkedDataFormatterTest.
 */

namespace Drupal\ld_tool\Tests;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\simpletest\KernelTestBase;

/**
 * Tests the ld_tool formatter.
 *
 * @group Linked_Data
 */
class LinkedDataFormatterTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array(
    'field',
    'entity_test',
    'system',
    'user',
    'ld_tool',
  );

  /**
   * @var string
   */
  protected $entityType;

  /**
   * @var string
   */
  protected $bundle;

  /**
   * @var string
   */
  protected $fieldName;

  /**
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $display;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Configure the theme system.
    $this->installConfig(array('system', 'field'));
    $this->installSchema('system', 'router');
    \Drupal::service('router.builder')->rebuild();
    $this->installEntitySchema('entity_test');

    $this->entityType = 'entity_test';
    $this->bundle = $this->entityType;
    $this->fieldName = Unicode::strtolower($this->randomMachineName());

    $field_storage = FieldStorageConfig::create(array(
      'field_name' => $this->fieldName,
      'entity_type' => $this->entityType,
      'type' => 'ld_tool',
    ));
    $field_storage->save();

    $instance = FieldConfig::create(array(
      'field_storage' => $field_storage,
      'bundle' => $this->bundle,
      'label' => $this->randomMachineName(),
      'settings' => array(
        'data_source' => 'wikidata',
      ),
    ));
    $instance->save();

    $this->display = entity_get_display($this->entityType, $this->bundle, 'default')
      ->setComponent($this->fieldName, array(
        'type' => 'ld_tool',
        'settings' => array(),
      ));
    $this->display->save();
  }

  /**
   * Renders fields of a given entity with a given display.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity object with attached fields to render.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The display to render the fields in.
   *
   * @return string
   *   The rendered entity fields.
   */
  protected function renderEntityFields(FieldableEntityInterface $entity, EntityViewDisplayInterface $display) {
    $content = $display->build($entity);
    $content = $this->render($content);
    return $content;
  }

  /**
   * Tests string formatter output.
   */
  public function testLinkedDataFormatter() {
    $uid = 'Q123'; $value = $this->randomMachineName();
    $link = "<a href=\"https://www.wikidata.org/entity/{$uid}\">{$value}</a>";

    $entity = EntityTest::create(array());
    $entity->{$this->fieldName}->uri = "[{$uid}] {$value}";
    $entity->{$this->fieldName}->source = "wikidata";

    // Verify that all HTML is escaped and newlines are retained.
    $this->renderEntityFields($entity, $this->display);
    $this->assertText($value);
    $this->assertRaw($link);
  }

}
