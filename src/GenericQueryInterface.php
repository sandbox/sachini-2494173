<?php

/**
 * @file
 * Contains \Drupal\ld_tool\GenericQueryAPI.
 */

namespace Drupal\ld_tool;
use Symfony\Component\HttpFoundation\Request;

/**
 * Generic API for querying for entities in Linked Data sources.
 */
interface GenericQueryInterface {

  /**
   * Returns display name of the Linked data_source.
   *
   * @return string
   *   Datasource class for this Handler.
   */
  public function getDataSourceName();

  /**
   * Query the API with the search string.
   *
   * @param string $search_string
   *   Query string.
   *
   * @return string
   *   JSON encoded string containing results.
   */
  public function queryForEntity($search_string);

  /**
   * Returns autocomplete options for the specific request.
   *
   * This method is to be referred by the ld_tool.get_<data_source> route.
   * The output should have id, text, description, image, notable and url
   * attributes foreach result item.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing autocomplete suggestions.
   */
  public function autocomplete(Request $request);

  /**
   * Build URL given the uid.
   *
   * @param string $uid
   *    Unique identifier for an entity in this Knowledge base.
   *
   * @return string
   *    Url for the entity.
   */
  public function getUrl($uid);

}
