<?php
/**
 * @file
 * Contains \Drupal\ld_tool\FreebaseQueryHandler.
 */

namespace Drupal\ld_tool;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Handler Class for Freebase (http://www.freebase.com/).
 */
class FreebaseQueryHandler implements GenericQueryInterface {
  protected $apiKey;

  protected $imageParams;

  /**
   * {@inheritdoc}
   */
  public function __construct() {

    $this->apiKey = \Drupal::config('ld_tool.api_key')->get('freebase');

    $this->imagePrams = array(
      'maxwidth' => 75,
    );

    if ($this->apiKey !== NULL) {
      $this->imageParams['key'] = $this->apiKey;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getDataSourceName() {

    return "Freebase";
  }

  /**
   * {@inheritdoc}
   */
  public function queryForEntity($search_string) {
    $service_url = 'https://www.googleapis.com/freebase/v1/search';
    $client = \Drupal::httpClient();
    $request = $client->createRequest('GET', $service_url);

    $query = $request->getQuery();
    $query->set('query', $search_string);
    if ($this->apiKey !== NULL) {
      $query->set('key', $this->apiKey);
    }
    $request->addHeader('Accept', 'application/json');

    $response = $client->send($request);
    $output = array();
    if ($response->getStatusCode() == 200) {
      foreach ($response->json()['result'] as $result) {
        echo $result['name'] . '<br/>';
        array_push($output, $result);
      }
    }

    return json_encode($output);
  }

  /**
   * {@inheritdoc}
   */
  public function autocomplete(Request $request) {
    $service_url = 'https://www.googleapis.com/freebase/v1/search';
    $image_url = 'https://www.googleapis.com/freebase/v1/image';

    $client = \Drupal::httpClient();
    $http_request = $client->createRequest('GET', $service_url);

    // Set parameters.
    $query = $http_request->getQuery();
    $query->set('query', $request->query->get('q'));
    $query->set('limit', 10);
    $query->set('output', '(description /common/topic/image)');
    if ($this->apiKey !== NULL) {
      $query->set('key', $this->apiKey);
    }

    $http_request->addHeader('Accept', 'application/json');
    $response = $client->send($http_request);

    $output = array();
    if ($response->getStatusCode() == 200) {
      foreach ($response->json()['result'] as $result) {
        $image = "";
        if (isset($result["output"]["/common/topic/image"], $result["output"]["/common/topic/image"]["/common/topic/image"])) {
          $image = $image_url . $result["output"]["/common/topic/image"]["/common/topic/image"][0]["mid"] . '?' . http_build_query($this->imageParams);
        }

        array_push($output, array(
            'id' => "[{$result['mid']}] {$result['name']}",
        // Or mid + name.
            'text' => $result['name'],
            'description' => empty($result["output"]['description']) ? "" : substr($result["output"]['description']["/common/topic/description"][0], 0, 245) . '..',
            'image' => $image,
            'notable' => isset($result["notable"]) ? $result["notable"]['name'] : "",
            'url' => $this->getUrl($result['mid']),
        ));
      }

    }

    return new JsonResponse($output);
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl($uid) {

    $prefix = "http://www.freebase.com";
    return $prefix . $uid;
  }

}
