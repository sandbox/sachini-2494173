<?php
/**
 * @file
 * Contains \Drupal\ld_tool\WikidataQueryHandler.
 */

namespace Drupal\ld_tool;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Handler Class for Wikidata (http://www.wikidata.org/).
 */
class WikidataQueryHandler implements GenericQueryInterface {
  protected $apiKey;

  /**
   * {@inheritdoc}
   */
  public function getDataSourceName() {
    return "Wikidata";
  }

  /**
   * {@inheritdoc}
   */
  public function queryForEntity($search_string) {

    $service_url = 'https://www.wikidata.org/w/api.php/';
    $client = \Drupal::httpClient();
    $http_request = $client->createRequest('GET', $service_url);

    // Set parameters.
    $query = $http_request->getQuery();
    $query->set('action', 'wbsearchentities');
    $query->set('search', $search_string);
    $query->set('language', 'en');
    $query->set('format', 'json');

    $http_request->addHeader('Accept', 'application/json');
    $response = $client->send($http_request);

    $output = array();
    if ($response->getStatusCode() == 200) {
      foreach ($response->json()['search'] as $result) {
        echo $result['label'] . '<br/>';
        array_push($output, $result);
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function autocomplete(Request $request) {

    $service_url = 'https://www.wikidata.org/w/api.php/';
    $image_url = "https://commons.wikimedia.org/wiki/Special:Redirect/file/";

    $client = \Drupal::httpClient();
    $http_request = $client->createRequest('GET', $service_url);

    // Set parameters.
    $query = $http_request->getQuery();
    $query->set('action', 'wbsearchentities');
    $query->set('search', $request->query->get('q'));
    $query->set('limit', 10);
    $query->set('language', 'en');
    $query->set('format', 'json');

    $http_request->addHeader('Accept', 'application/json');
    $response = $client->send($http_request);

    $output = array();
    if ($response->getStatusCode() == 200) {
      foreach ($response->json()['search'] as $result) {
        $image_request = $client->createRequest('GET', $service_url);

        // Set parameters.
        $image_query = $image_request->getQuery();
        $image_query->set('action', 'wbgetentities');
        $image_query->set('ids', $result['id']);
        $image_query->set('props', 'claims|descriptions');
        $image_query->set('language', 'en');
        $image_query->set('format', 'json');

        $image_request->addHeader('Accept', 'application/json');
        $image_response = $client->send($image_request);

        $image = "";
        if ($image_response->getStatusCode() == 200) {
          $entity = $image_response->json();
          if (isset($entity['entities'][$result['id']]['claims']['P18'])) {
            $image_name = $entity['entities'][$result['id']]['claims']['P18'][0]['mainsnak']['datavalue']['value'];
            $image = $image_url . str_replace(' ', '%20', $image_name) . "?width=75px";
          }
        }

        array_push($output, array(
            'id' => "[{$result['id']}] {$result['label']}",
        // Or mid + name.
            'text' => $result['label'],
            'description' => isset($result['description']) ? $result['description'] : "",
            'image' => $image,
            'notable' => isset($result["notable"]) ? $result["notable"]['name'] : "",
            'url' => $this->getUrl($result['id']),
        ));
      }
    }

    return new JsonResponse($output);
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl($uid) {

    $prefix = "https://www.wikidata.org/entity/";
    return $prefix . $uid;
  }

}
