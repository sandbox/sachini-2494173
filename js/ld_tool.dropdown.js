/**
 * @file
 * Attach select2 to a dropdown element.
 */

(function ($, Drupal) {
    Drupal.behaviors.ld_tool = {
        attach: function (context, settings) {
            $('select.ld-selector', context).select2();
        }
    }
})(jQuery, Drupal);
