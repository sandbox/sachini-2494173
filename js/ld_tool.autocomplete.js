/**
 * @file
 * Attach select2 to dropdown element with remote data as options..
 */

(function ($, Drupal, drupalSettings) {
  /*Drupal.behaviors.ld_tool = {
      attach: function (context) {
          // Act on textfields with the "form-autocomplete" class.
          var $autocomplete = $(context).find('input.form-autocomplete').once('autocomplete');
          alert($autocomplete.attr('id'));
      }
  }*/
    Drupal.behaviors.ld_tool = {
        attach: function (context, settings) {
            var $dropdown = $('select.ld-autocomplete', context).select2({
                placeholder: drupalSettings.ld_tool.placeholder,
                minimumInputLength: 2,
                ajax: {
                    url: Drupal.url(drupalSettings.ld_tool.url),
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                          // Search term.
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // Parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used.
                        params.page = params.page || 1;
                        return {
                            results: data
                          /*    text: "label"*/
                        };
                    }
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                templateResult: formatData,
                templateSelection: formatDataSelection
            });

            if (drupalSettings.ld_tool.defaultVal) {
                var option = new Option(drupalSettings.ld_tool.defaultVal, drupalSettings.ld_tool.defaultVal, true, true);
                $dropdown.append(option);
                $dropdown.trigger('change');
            }

            function formatData (data) {
                if (data.loading) {
                return data.text; }

                var image = "";
                if (data.image) {
                  image = '<img class="select-option" src="' + data.image + '" style="max-width: 100%; float: left; margin-right: 10px;" />';
                }

                var markup = '<div class="clearfix col-sm-12">' +
                    image +
                    '<div>' +
                    '<div><b>' + data.text + '</b></div>' +
                    '<div><i>' + data.notable + '</i></div>' +
                    '<div>' + data.description + '</div>' +
                    '</div></div>';

                return markup;
            }

            function formatDataSelection (data) {
                return data.id;
            }
        }
    }
})(jQuery, Drupal, drupalSettings);
