<?php

/**
 * @file
 * Contains \Drupal\ld_select\Form\LinkedDataDialog.
 */

namespace Drupal\ld_select\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\filter\Entity\FilterFormat;

/**
 * Provides a link dialog for text editors.
 */
class LinkedDataDialog extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'linked_data_dialog';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\filter\Entity\FilterFormat $filter_format
   *   The filter format for which this dialog corresponds.
   */
  public function buildForm(array $form, FormStateInterface $form_state, FilterFormat $filter_format = NULL) {

    // The default values are set directly from \Drupal::request()->request,
    // provided by the editor plugin opening the dialog.
    $user_input = $form_state->getUserInput();

    $input = isset($user_input['editor_object']) ? $user_input['editor_object']['href'] : "";
    $selectedText = isset($user_input['editor_object']) ? $user_input['editor_object']['text-for-tagging'] : "";

    $form['#tree'] = TRUE;
    $form['#attached']['library'] = array(
      'editor/drupal.editor.dialog',
      'ld_select/drupal.ld_select.autocomplete',
    );

    $form['#prefix'] = '<div id="linked-data-dialog-form">';
    $form['#suffix'] = '</div>';

    $datasources = $this->getDatasourceOptions($input);

    $form['datasource'] = array(
      '#title' => $this->t('Linked Data Source'),
      '#type' => 'select',
      '#default_value' => isset($datasources['default']) ? $datasources['default'] : '',
      '#validated' => TRUE,
      '#options' => $datasources['options'],
      '#attributes' => array(
        'id' => "ld-select-datasource-select",
      ),
    );

    // Everything under the "attributes" key is merged directly into the
    // generated link tag's attributes.
    $form['attributes']['href'] = array(
      '#title' => $this->t('Linked Data Item'),
      '#type' => 'select',
      '#maxlength' => 2048,
      '#validated' => TRUE,
      '#attached' => array(
        'drupalSettings' => array(
          'ld_select' => array(
            'default_value' => $input,
            'selected_text' => $selectedText,
          ),
        ),
      ),
      '#attributes' => array(
        'class' => array('ld-selector'),
        'id' => "ld-select-data-item-select",
      ),
    );

    $form['actions'] = array(
      '#type' => 'actions',
    );
    $form['actions']['save_modal'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      // No regular submit-handler. This form only works via JavaScript.
      '#submit' => array(),
      '#ajax' => array(
        'callback' => '::submitForm',
        'event' => 'click',
      ),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $response = new AjaxResponse();

    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
          '#type' => 'status_messages',
          '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#linked-data-dialog-form', $form));
    }
    else {
      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

  /**
   * Get datasources list and default value for datasource.
   *
   * Datasource list is formatted so that for each element the option is
   * url => Human Readable label.
   *
   * @param string $input
   *   Input url if a value is already set.
   *
   * @return array
   *   Datasource list and default url.
   */
  protected function getDatasourceOptions($input) {

    $default = "";
    $datasource_options = array();
    $datasources = get_knowledge_bases();

    foreach ($datasources as $key => $value) {
      $url = Url::fromRoute(get_knowledge_base_route($key));
      $url = substr($url->toString(), 1);
      $datasource_options[$url] = $value;
      if (!empty($input)) {
        if (strpos($input, $key) !== FALSE) {
          $default = $url;
        }
      }
    }

    return array('options' => $datasource_options, 'default' => $default);
  }

}
