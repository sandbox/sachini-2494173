/**
 * @file
 * Defines Autocomplete combobox for RDF mappings.
 */

(function ($, Drupal, drupalSettings) {
     Drupal.behaviors.ld_select = {
        attach: function (context, settings) {

            // Focus to select2 widget.
            if ($.ui && $.ui.dialog && $.ui.dialog.prototype._allowInteraction) {
                var ui_dialog_interaction = $.ui.dialog.prototype._allowInteraction;
                $.ui.dialog.prototype._allowInteraction = function(e) {
                    if ($(e.target).closest('.select2-dropdown').length) {
                    return true; }
                    return ui_dialog_interaction.apply(this, arguments);
                };
            }

            var $datasource_dropdown = $('#ld-select-datasource-select', context).select2();

            var $item_dropdown = $('#ld-select-data-item-select', context).select2({
                minimumInputLength: 2,
                ajax: {
                    url: function() { return Drupal.url($('#ld-select-datasource-select').val())},
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                          // Search term.
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // Parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used.
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.id,
                                    id: item.url,
                                    image: item.image,
                                    description: item.description,
                                    notable: item.notable,
                                    label: item.text
                                }
                            })
                        };
                    }
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                templateResult: formatData
            });

            /* Set selected text as the search term on open.*/
            $item_dropdown.on("select2:open", function() {
                var $search = $item_dropdown.data('select2').dropdown.$search;
                $search.val(drupalSettings.ld_select.selected_text);
                $search.trigger('keyup');
            });

            if (drupalSettings.ld_select.default_value) {
                var option = new Option(drupalSettings.ld_select.default_value, drupalSettings.ld_select.default_value, true, true);
                $item_dropdown.append(option);
                $item_dropdown.trigger('change');
            }

            function formatData (data) {
                if (data.loading) {
                return data.text; }

                var image = "";
                if (data.image) {
                  image = '<img class="select-option" src="' + data.image + '" style="max-width: 100%; float: left; margin-right: 10px;" />';
                }

                var markup = '<div class="clearfix col-sm-12">' +
                    image +
                    '<div>' +
                    '<div><b>' + data.label + '</b></div>' +
                    '<div><i>' + data.notable + '</i></div>' +
                    '<div>' + data.description + '</div>' +
                    '</div></div>';

                return markup;
            }
        }
    }
})(jQuery, Drupal, drupalSettings);
